import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as patches
import csv

def colors(player):
    if 'Hombre' in player:
        return 0
    elif 'Mujer' in player:
        return 1
    elif 'Mujer' in player:
        return 1
    elif 'Hombre' in player:
        return 0
    else:
        return "grey"



def piespot(x, y, s, ratio, ax, colors):

    i = [0] + np.cos(np.linspace(0,
                                 2 * np.pi * ratio,
                                 20)).tolist()

    j = [0] + np.sin(np.linspace(0,
                                 2 * np.pi * ratio,
                                 20)).tolist()

    xy1 = np.column_stack([i, j])
    s1 = np.abs(xy1).max()

    i = [0] + np.cos(np.linspace(2 * np.pi * ratio,
                                 2 * np.pi,
                                 20)).tolist()
    j = [0] + np.sin(np.linspace(2 * np.pi * ratio,
                                 2 * np.pi,
                                 20)).tolist()
    xy2 = np.column_stack([i, j])
    s2 = np.abs(xy2).max()

    ax.scatter(x, y, marker=xy1,
               s=s, facecolor=colors[1])

    ax.scatter(x, y, marker=xy2,
               s=s, facecolor=colors[0])



def scatter_pie(f, outpath, col=('blue', 'red')):
    plt.cla()
    fig, ax = plt.subplots()
    reader = csv.reader(open(f, 'r'))

    # sum repeats for bubble sizes
    spot = {}
    for row in reader:
        (player, ind, fam) = row
        w = spot.get((ind, fam), {'w': 0,
                                  'colors': []})
        w['w'] += 1
        w['colors'].append(colors(player))
        spot[(ind, fam)] = w

    # create arrays for each spot
    X = []
    Y = []
    Z = []
    color = []
    for k in spot:
        x = int(k[0])
        y = int(k[1])
        s = spot[k]['w'] * 77
        ratio = np.mean(spot[k]['colors'])
        piespot(x, y, s, ratio, ax=ax, colors=col)

    ax.set_aspect('equal')
    ax.set_xlabel('individual')
    ax.set_ylabel('familiar')

    # diagonal
    line = mlines.Line2D([0, 1], [0, 1], color='forestgreen', linewidth=0.2)
    transform = ax.transAxes
    line.set_transform(transform)
    ax.add_line(line)

    # next three mlines are broken stick
    ax.add_line(
        mlines.Line2D(
            [2, -1],
            [6, 8],
            color='firebrick', linewidth=0.2))

    ax.add_line(
        mlines.Line2D(
            [2, 6],
            [6, 2],
            color='firebrick', linewidth=0.2))

    ax.add_line(
        mlines.Line2D(
            [6, 8],
            [2, -1],
            color='firebrick', linewidth=0.2))

    # grey rectangle
    ax.add_patch(
        patches.Rectangle(
            (-1, -1), 5, 5,
            color='grey', alpha=0.1, linewidth=0))

    plt.xlim(-0.4, 7.4)
    plt.ylim(-0.4, 7.4)

    plt.savefig(outpath)



files = [
    ('data/a_j1.csv', 'figs/scatterpie_a_j1.png', ('royalblue', 'crimson')),
    ('data/a_j2.csv', 'figs/scatterpie_a_j2.png', ('royalblue', 'crimson')),
    ('data/a_j3.csv', 'figs/scatterpie_a_j3.png', ('royalblue', 'crimson')),
    ('data/j_j1.csv', 'figs/scatterpie_j_j1.png', ('deepskyblue', 'deeppink')),
    ('data/j_j2.csv', 'figs/scatterpie_j_j2.png', ('deepskyblue', 'deeppink')),
    ('data/j_j3.csv', 'figs/scatterpie_j_j3.png', ('deepskyblue', 'deeppink')),
]


for f in files:
    scatter_pie(f[0], f[1], col=f[2])

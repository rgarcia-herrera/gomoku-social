import matplotlib.pyplot as plt
import csv


def pie(inpath, outpath, colors):
    reader = csv.reader(open(inpath, 'r'))

    female = 0
    male = 0
    for row in reader:
        (player, ind, fam) = row
        if player == 'Mujer':
            female += 1
        elif player == 'Hombre':
            male += 1
        else:
            exit(1)

    labels = 'Mujer', 'Hombre'

    sizes = [female, male]

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes, colors=colors, labels=labels, autopct='%1.1f%%',
            startangle=90)
    ax1.axis('equal')

    plt.savefig(outpath)



files = [
    ('data/a_j1.csv', 'figs/genre_pie_a.png', ['crimson', 'royalblue']),
    ('data/j_j1.csv', 'figs/genre_pie_j.png', ['deeppink', 'skyblue']),
]

for f in files:
    pie(*f)

import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import matplotlib.patches as patches

def plot(f, title, outpath):
    reader = csv.reader(open(f, 'r'))

    # sum repeats for bubble sizes
    spot = {}
    for row in reader:
        (player, ind, fam) = row
        w = spot.get((ind, fam), {'w': 0})
        w['w'] += 1
        spot[(ind, fam)] = w

    # create arrays for each spot
    X = []
    Y = []
    Z = []

    for k in spot:
        X.append(int(k[0]))
        Y.append(int(k[1]))
        Z.append(spot[k]['w'] * 77)

    # plot 'em
    fig = plt.figure()
    plt.scatter(X, Y, Z, color='grey', linewidth=0)

    ax = fig.gca()
    ax.set_aspect('equal')

    ax.set_xlabel('individual')
    ax.set_ylabel('familiar')

    # diagonal
    line = mlines.Line2D([0, 1], [0, 1], color='forestgreen', linewidth=0.2)
    transform = ax.transAxes
    line.set_transform(transform)
    ax.add_line(line)

    # next three mlines are broken stick
    ax.add_line(
        mlines.Line2D(
            [2, -1],
            [6, 8],
            color='firebrick', linewidth=0.2))

    ax.add_line(
        mlines.Line2D(
            [2, 6],
            [6, 2],
            color='firebrick', linewidth=0.2))

    ax.add_line(
        mlines.Line2D(
            [6, 8],
            [2, -1],
            color='firebrick', linewidth=0.2))

    # grey rectangle
    ax.add_patch(
        patches.Rectangle(
            (-1, -1), 5, 5,
            color='grey', alpha=0.1, linewidth=0))


    plt.xlim(-0.4, 7.4)
    plt.ylim(-0.4, 7.4)

    plt.savefig(outpath)


files = [
    ('data/a_j1.csv', 'a_j1', 'figs/a_j1_nogenre.png'),
    ('data/a_j2.csv', 'a_j2', 'figs/a_j2_nogenre.png'),
    ('data/a_j3.csv', 'a_j3', 'figs/a_j3_nogenre.png'),
    ('data/j_j1.csv', 'j_j1', 'figs/j_j1_nogenre.png'),
    ('data/j_j2.csv', 'j_j2', 'figs/j_j2_nogenre.png'),
    ('data/j_j3.csv', 'j_j3', 'figs/j_j3_nogenre.png'),
]

for f in files:
    plot(*f)

<table>
  <tr>
	<th>adultos</th>
    <th>jóvenes</th>
  </tr>
  <tr>
	<td><img width="90%" src="genre_pie_a.png"></td>
	<td><img width="90%" src="genre_pie_j.png"></td>
  </tr>
</table>

----

<table>
  <tr>
	<td>&nbsp;</td>
	<th>1</th>
	<th>2</th>
	<th>3</th>
  <tr>
	<th>adulto</th>
	<td><img width="100%" src="scatterpie_a_j1.png"></td>
	<td><img width="100%" src="scatterpie_a_j2.png"></td>
	<td><img width="100%" src="scatterpie_a_j3.png"></td>
  </tr>

  <tr>
	<th>joven</th>
	<td><img width="100%" src="scatterpie_j_j1.png"></td>
	<td><img width="100%" src="scatterpie_j_j2.png"></td>
	<td><img width="100%" src="scatterpie_j_j3.png"></td>
  </tr>
</table>


<table>
  <tr>
	<td>&nbsp;</td>
	<th>1</th>
	<th>2</th>
	<th>3</th>
  <tr>
	<th>adulto</th>
	<td><img width="100%" src="a_j1_nogenre.png"></td>
	<td><img width="100%" src="a_j2_nogenre.png"></td>
	<td><img width="100%" src="a_j3_nogenre.png"></td>
  </tr>

  <tr>
	<th>joven</th>
	<td><img width="100%" src="j_j1_nogenre.png"></td>
	<td><img width="100%" src="j_j2_nogenre.png"></td>
	<td><img width="100%" src="j_j3_nogenre.png"></td>
  </tr>
</table>


<table style="width:100%">
  <tr>
	<td>&nbsp;</td>
	<th>1</th>
	<th>2</th>
	<th>3</th>
  <tr>
	<th>adulto</th>
	<td><img width="100%" src="a_j1.png"></td>
	<td><img width="100%" src="a_j2.png"></td>
	<td><img width="100%" src="a_j3.png"></td>
  </tr>

  <tr>
	<th>joven</th>
	<td><img width="100%" src="j_j1.png"></td>
	<td><img width="100%" src="j_j2.png"></td>
	<td><img width="100%" src="j_j3.png"></td>
  </tr>
</table>
